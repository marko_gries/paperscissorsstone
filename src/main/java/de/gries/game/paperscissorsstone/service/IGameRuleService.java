package de.gries.game.paperscissorsstone.service;

import de.gries.game.paperscissorsstone.model.Symbol;

/**
 * A rule for the game the game PAPER, SCISSORS, STONE
 */
public interface IGameRuleService {

    /**
     * Takes two {@link Symbol} and determines which of them is the winner.
     * If there is no winner {@link Symbol#NONE} is returned.
     *
     * @param symbolOne The first symbol to compare to the second one.
     * @param symbolTwo The second symbol to compare to the first one.
     * @return Return one of the given symbols which is winner, otherwise NONE.
     */
    public Symbol determineWinnerSymbol(Symbol symbolOne, Symbol symbolTwo);
}
