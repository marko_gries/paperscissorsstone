package de.gries.game.paperscissorsstone.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.gries.game.paperscissorsstone.model.Match;
import de.gries.game.paperscissorsstone.model.Pair;
import de.gries.game.paperscissorsstone.model.Player;
import de.gries.game.paperscissorsstone.model.PlayerName;
import de.gries.game.paperscissorsstone.model.Symbol;
import de.gries.game.paperscissorsstone.view.MatchResult;

/**
 * Service for game PAPER, SCISSORS, STONE.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
@Service
public class GameService {

    @Autowired
    private IGameRuleService gameRule;

    private final int STRATEGY_PLAYER_TWO = 1;

    /**
     * Starts the game PAPER, SCISSORS, STONE and returns a collected result
     * of all matches that were played within this game.
     * To play a game a strategy and the count of matches are needed.
     * The strategy determines how the first player of the game acts.
     * The first player can act always as a STONE or can be chosen by random.
     * After the game is finished all results of the matches are returned.
     *
     * @param strategy Strategy can only be 1 = random otherwise always stone.
     * @param countOfMatches Number of matches that should be played.
     * @return {@link MatchResult} as a list of all results.
     */
    public List<MatchResult> playGame(int strategy, int countOfMatches) {
        List<MatchResult> matchResultList = new ArrayList<>();
        for (int i = 0; i < countOfMatches; i++) {
            Match match = playMatch(strategy);
            matchResultList.add(getMatchResult(match));
        }
        return matchResultList;
    }

    /**
     * Play one {@link Match} of the game.
     * A strategy is needed to play a match for determining the symbol
     * of the first {@link Player}.
     * After a symbol is chosen for each of both players the symbols are
     * compared and the player with the stronger symbol is the winner of
     * the match.
     *
     * @param strategy Strategy can only be 1 = random otherwise always STONE.
     * @return Return a match with the two players and the match winner of type {@link Player}
     */
    public Match playMatch(int strategy) {

        Player playerOne = new Player(PlayerName.ONE);
        Player playerTwo = new Player(PlayerName.TWO);
        Pair<Player, Player> playerPair = new Pair<>(playerOne, playerTwo);
        Match match = new Match(playerPair);

        // Choose and set symbol of player one regarding of given strategy.
        playerOne.setSymbol(chooseSymbol(strategy));

        // Choose and set symbol of player two. The strategy for player two is always 1 = random
        playerTwo.setSymbol(chooseSymbol(STRATEGY_PLAYER_TWO));

        // And the winner is ...
        Player matchWinner = determineMatchWinner(playerOne, playerTwo);
        match.setMatchWinner(matchWinner);

        return match;
    }

    /**
     * Determines the winner of a {@link Match}:
     * With the two {@link Symbol} of each given {@link Player} a rule is inquired.
     * The rule determines the winner of both symbols and returns it.
     * <p>
     * If the symbol of player one is equal to the returned winner symbol, player one
     * is set as match winner.
     * If the symbol of player two is equal to the returned winner symbol, player two
     * is set as match winner.
     * If either the symbol of player one nor the symbol of player two is equal
     * to the returned winner symbol a new player with name {@link PlayerName#NONE}
     * is set as match winner.
     *
     * @param playerOne The first player with a symbol.
     * @param playerTwo The second player with a symbol.
     * @return  If the match has a winner the according player is returned,
     *          otherwise a new player with player name = none.
     */
    private Player determineMatchWinner(Player playerOne, Player playerTwo) {
        Symbol symbolWinner = gameRule.determineWinnerSymbol(
                playerOne.getSymbol(), playerTwo.getSymbol());

        Player matchWinner;
        if (symbolWinner.equals(playerOne.getSymbol())) {
            matchWinner = playerOne;
        } else if(symbolWinner.equals(playerTwo.getSymbol())) {
            matchWinner = playerTwo;
        } else {
            matchWinner = new Player(PlayerName.NONE);
        }

        return matchWinner;
    }

    /**
     * Determines the {@link Symbol} based on a given strategy.
     *
     * @param strategy Strategy can only be 1 = random otherwise always default.
     * @return Return a {@link Symbol} which represents a PAPER, SCISSORS or STONE.
     */
    private Symbol chooseSymbol(int strategy) {
        switch (strategy) {
            case 1:
                int symbolNumber = (int) (Math.random() * 3 + 1);
                return Symbol.getSymbol(symbolNumber);
            default:
                return Symbol.getDefaultSymbol();
        }
    }

    private MatchResult getMatchResult(Match match) {
        MatchResult matchResult = new MatchResult();
        matchResult.setSymbolNamePlayerOne(match.getPlayerOne().getSymbol().name());
        matchResult.setSymbolNamePlayerTwo(match.getPlayerTwo().getSymbol().name());
        matchResult.setPlayerNameWinner(match.getMatchWinner().getPlayerName());
        return matchResult;
    }
}
