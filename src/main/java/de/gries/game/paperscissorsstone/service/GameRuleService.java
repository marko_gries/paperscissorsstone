package de.gries.game.paperscissorsstone.service;

import org.springframework.stereotype.Component;

import de.gries.game.paperscissorsstone.model.Pair;
import de.gries.game.paperscissorsstone.model.Symbol;

/**
 * The rule for the game the game PAPER, SCISSORS, STONE.
 * Determines the winner of two given {@link Symbol} by a fix rule mapping.
 * The rule is taken from <a href="https://de.wikipedia.org/wiki/Schere,_Stein,_Papier</a>
 *
 * @author Marko Gries
 * @date 6.10.2018
 */
@Component
public class GameRuleService implements IGameRuleService {

    public Symbol determineWinnerSymbol(Symbol symbolOne, Symbol symbolTwo) {

        Pair<Symbol, Symbol> pair = determineStrongAndWeakSymbolFor(symbolOne);

        Symbol symbolStrong = pair.getFirst();
        Symbol symbolWeak = pair.getSecond();

        Symbol symbolWinner;
        if (symbolStrong.equals(symbolTwo)) {
            symbolWinner = symbolTwo;
        } else if (symbolWeak.equals(symbolTwo)) {
            symbolWinner = symbolOne;
        } else {
            symbolWinner = Symbol.NONE;
        }

        return symbolWinner;
    }

    /**
     * Determines a {@link Pair} of {@link Symbol} which are stronger respectively weaker as given symbol.
     *
     * Each symbol has an injective mapping to other symbols.
     * Dependent of the given symbol two other symbols are determined:
     * <p>
     * The first symbol is the stronger symbol
     * -> the given symbol loose in comparison to this symbol.
     * <p>
     * The second symbol is the weaker symbol
     * -> the given symbol wins in comparison to this symbol.
     * <p>
     * How to use the rule:
     * When a symbol is given, the stronger and weaker symbols are determined
     * and returned as a pair. Each other symbol can now be compared
     * to this pair of symbols.
     * <p>
     * Example:
     * Symbols to play with:
     * First symbol = STONE
     * Second symbol = PAPER
     *
     * Now for the first symbol the stronger and weaker symbols are determined:
     * Stronger symbol = PAPER (paper can wrap a stone)
     * Weaker symbol = SCISSORS (a stone can blunt a scissors)
     *
     * After comparison the second symbol(paper) with the returned two symbols (stronger, weaker)
     * it is obvious that the second symbol wins.
     *
     * @param symbol The symbol for determine the mapping of stronger and weaker symbols.
     * @return Return a pair of symbols which are stronger respectively weaker as given symbol.
     */
    private Pair<Symbol, Symbol> determineStrongAndWeakSymbolFor(Symbol symbol) {

        Symbol symbolStrong = Symbol.NONE;
        Symbol symbolWeak = Symbol.NONE;

        switch(symbol) {
            case SCISSORS:
                symbolStrong = Symbol.STONE;
                symbolWeak = Symbol.PAPER;
                break;
            case STONE:
                symbolStrong = Symbol.PAPER;
                symbolWeak = Symbol.SCISSORS;
                break;
            case PAPER:
                symbolStrong = Symbol.SCISSORS;
                symbolWeak = Symbol.STONE;
        }

        return new Pair(symbolStrong, symbolWeak);
    }
}
