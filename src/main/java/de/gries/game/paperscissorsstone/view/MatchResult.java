package de.gries.game.paperscissorsstone.view;

import de.gries.game.paperscissorsstone.model.Match;

/**
 * Match result for PAPER, SCISSORS, STONE.
 * The match result holds name of the players who compete
 * and also the name of the match winner.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class MatchResult {

    private String symbolNamePlayerOne;
    private String symbolNamePlayerTwo;
    private String playerNameWinner;

    public String getSymbolNamePlayerOne() {
        return symbolNamePlayerOne;
    }

    public void setSymbolNamePlayerOne(String symbolNamePlayerOne) {
        this.symbolNamePlayerOne = symbolNamePlayerOne;
    }

    public String getSymbolNamePlayerTwo() {
        return symbolNamePlayerTwo;
    }

    public void setSymbolNamePlayerTwo(String symbolNamePlayerTwo) {
        this.symbolNamePlayerTwo = symbolNamePlayerTwo;
    }

    public String getPlayerNameWinner() {
        return playerNameWinner;
    }

    public void setPlayerNameWinner(String playerNameWinner) {
        this.playerNameWinner = playerNameWinner;
    }
}
