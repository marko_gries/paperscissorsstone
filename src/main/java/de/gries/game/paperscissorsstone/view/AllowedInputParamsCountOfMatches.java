package de.gries.game.paperscissorsstone.view;

/**
 * Possible input parameters for the count of matches that should be played within this game.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class AllowedInputParamsCountOfMatches implements IAllowedInputParams {

    private final int MIN_COUNT_OF_MATCHES = 1;
    private final int MAX_COUNT_OF_MATCHES = Integer.MAX_VALUE;
    private final String PROMPT_COUNT_OF_MATCHES = "Please type in the desired count of matches(min 1): ";

    @Override
    public int getValidMin() {
        return MIN_COUNT_OF_MATCHES;
    }

    @Override
    public int getValidMax() {
        return MAX_COUNT_OF_MATCHES;
    }

    @Override
    public String getPromptMessage() {
        return PROMPT_COUNT_OF_MATCHES;
    }
}
