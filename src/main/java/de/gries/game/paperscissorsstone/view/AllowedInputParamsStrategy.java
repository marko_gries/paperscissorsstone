package de.gries.game.paperscissorsstone.view;

/**
 * Possible input parameters for the game strategy.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class AllowedInputParamsStrategy implements IAllowedInputParams {

    private final int MIN_NUMBER_STRATEGY = 1;
    private final int MAX_NUMBER_STRATEGY = 2;
    private final String PROMPT_STRATEGY = "Please type in your favorite strategy: 1 = random; 2 = always STONE";

    @Override
    public int getValidMin() {
        return MIN_NUMBER_STRATEGY;
    }

    @Override
    public int getValidMax() {
        return MAX_NUMBER_STRATEGY;
    }

    @Override
    public String getPromptMessage() {
        return PROMPT_STRATEGY;
    }
}
