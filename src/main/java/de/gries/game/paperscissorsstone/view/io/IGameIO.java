package de.gries.game.paperscissorsstone.view.io;

/**
 * Interface for I/O of the game PAPER, SCISSORS, STONE.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public interface IGameIO {

    /**
     * Writes a given message to out.
     *
     * @param message
     */
    void writeMessage(String message);

    /**
     * Reads an Integer from In.
     *
     * @return Return an integer if found in In.
     */
    public int getIntFromIn();
}
