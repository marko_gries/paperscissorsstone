package de.gries.game.paperscissorsstone.view;

/**
 * Possible parameters for user input.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public interface IAllowedInputParams {

    int getValidMin();

    int getValidMax();

    String getPromptMessage();
}
