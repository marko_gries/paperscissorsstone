package de.gries.game.paperscissorsstone.view.validate;

import org.springframework.stereotype.Component;

import de.gries.game.paperscissorsstone.view.IAllowedInputParams;

/**
 * Validator for input made by user.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
@Component
public class AllowedInputValidator {

    /**
     * Validates the given parameters against an allowed range.
     *
     * @param input Given parameter from user.
     * @param allowedInputParams Possible range for given parameters.
     * @return Return true if given parameter is in range.
     */
    public boolean validateIntInput(int input, IAllowedInputParams allowedInputParams) {
        return !(input < allowedInputParams.getValidMin() || input > allowedInputParams.getValidMax());
    }
}
