package de.gries.game.paperscissorsstone.view.io;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.springframework.stereotype.Component;

/**
 * Simple abstraction for in- and output of the game PAPER, SCISSORS, STONE.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
@Component
public class GameIO implements IGameIO {

    @Override
    public void writeMessage(String message) {
        writeTo().println(message);
    }

    @Override
    public int getIntFromIn() {
        int intFromIn = 0;
        Scanner scanner = new Scanner(readFrom());
        if (scanner.hasNextInt()) {
            intFromIn = scanner.nextInt();
        }
        return intFromIn;
    }

    private PrintStream writeTo() {
        return System.out;
    }

    private InputStream readFrom() {
        return System.in;
    }
}
