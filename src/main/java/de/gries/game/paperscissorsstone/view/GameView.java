package de.gries.game.paperscissorsstone.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.gries.game.paperscissorsstone.view.io.IGameIO;
import de.gries.game.paperscissorsstone.view.validate.AllowedInputValidator;

/**
 * View to read and display information which are needed for user interaction.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
@Component
public class GameView {

    @Autowired
    private IGameIO gameIO;

    @Autowired
    private AllowedInputValidator inputValidator;

    private final String WARN_MESSAGE = "Wrong input, please type in correct values!";


    /**
     * Read chosen strategy from user.
     * The strategy determines the behaviour of the first player of the game.
     *
     * @return Return the strategy represented as int.
     */
    public int readStrategy() {
        IAllowedInputParams InputParamsStrategy = new AllowedInputParamsStrategy();
        return readIntFromUserInput(InputParamsStrategy);
    }

    /**
     * Reads the count of matches that should be played for the game.
     * At least one match must be given.
     *
     * @return Return count of matches.
     */
    public int readCountOfMatches() {
        IAllowedInputParams InputParamsCountOfMatches = new AllowedInputParamsCountOfMatches();
        return readIntFromUserInput(InputParamsCountOfMatches);
    }

    /**
     * Prints the results line by line of all matches which were played in this game.
     *
     * @param matchResultList The collected result of all matches within this game.
     */
    public void printGameResult(List<MatchResult> matchResultList) {
        printMessage("Result of all matches:");
        matchResultList.forEach(result -> gameIO.writeMessage(getComposedMatchResult(result)));
    }

    private String getComposedMatchResult(MatchResult matchResult) {
        return String.format("Player one has symbol %s and player two has symbol %s; %s wins!",
                matchResult.getSymbolNamePlayerOne(),
                matchResult.getSymbolNamePlayerTwo(),
                matchResult.getPlayerNameWinner());
    }

    /**
     * Prints out a given message.
     *
     * @param message
     */
    public void printMessage(String message) {
        gameIO.writeMessage(message);
    }

    private int readIntFromUserInput(IAllowedInputParams inputParams) {
        printMessage(inputParams.getPromptMessage());
        int input = readIntFromInput();
        while (!inputValidator.validateIntInput(input, inputParams)) {
            printMessage(WARN_MESSAGE);
            input = readIntFromInput();
        }
        return input;
    }

    private int readIntFromInput() {
        return gameIO.getIntFromIn();
    }
}
