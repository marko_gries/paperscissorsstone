package de.gries.game.paperscissorsstone;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import de.gries.game.paperscissorsstone.service.GameService;
import de.gries.game.paperscissorsstone.view.MatchResult;
import de.gries.game.paperscissorsstone.view.GameView;

/**
 * Starting point for this application.
 * Implements the {@link CommandLineRunner#run(String...)} method
 * to have an user interaction for simple in- and output.
 *
 * @author Marko Gries
 * @date 06.10.2018
 * @version 1.0
 */
@Component
public class UserInteraction implements CommandLineRunner {


    @Autowired
    private GameView gameView;

    @Autowired
    private GameService gameService;


    @Override
    public void run(String... strings) {
        gameView.printMessage("Let's start game PAPER, SCISSORS, STONE!");
        int strategy = gameView.readStrategy();
        int countOfGames = gameView.readCountOfMatches();

        List<MatchResult> matchResultList = gameService.playGame(strategy, countOfGames);
        gameView.printGameResult(matchResultList);
    }
}
