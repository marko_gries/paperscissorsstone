package de.gries.game.paperscissorsstone;

import java.util.Arrays;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PaperscissorsstoneApplication {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(PaperscissorsstoneApplication.class);

        //disable banner, don't want to see spring logo
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
}
