package de.gries.game.paperscissorsstone.model;

import java.util.Arrays;

/**
 * A {@link Symbol} represents the choice of a {@link Player}
 * in one {@link Match} of the game PAPER, SCISSORS, STONE.
 *
 * The symbols are ordered by a particular symbol number.
 * Each symbol can be identified by this number.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public enum Symbol {

    NONE(0),
    PAPER(1),
    SCISSORS(2),
    STONE(3);

    private final int symbolNumber;

    Symbol(int symbolNumber) {
        this.symbolNumber = symbolNumber;
    }

    /**
     * Delivers the symbol identified by a dedicated number.
     *
     * @param symbolNumber A specific number which is related to a symbol.
     * @return Return the related {@link Symbol} to a given number; otherwise null.
     */
    public static Symbol getSymbol(int symbolNumber) {
        return Arrays.stream(Symbol.values())
                .filter(s -> s.symbolNumber == symbolNumber)
                .findFirst()
                .orElse(null);
    }

    public static Symbol getDefaultSymbol() {
        return Symbol.STONE;
    }
}
