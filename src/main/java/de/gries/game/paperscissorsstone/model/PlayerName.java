package de.gries.game.paperscissorsstone.model;

/**
 * A player name represents the name of a {@link Player}
 * in one {@link Match} of the game PAPER, SCISSORS, STONE.
 * If a player has no name than it is set to {@link PlayerName#NONE}.
 * A player with no name can occur if a match has no match winner.
 * Than the winner of the match is a player with no name ...
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public enum PlayerName {

    ONE("Player one"),
    TWO("Player two"),
    NONE("No player");

    private String nameLiteral;

    PlayerName(String nameLiteral) {
        this.nameLiteral = nameLiteral;
    }

    public String getNameLiterally() {
        return nameLiteral;
    }
}
