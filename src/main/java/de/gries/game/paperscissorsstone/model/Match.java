package de.gries.game.paperscissorsstone.model;

/**
 * A match for the game PAPER, SCISSORS, STONE.
 * One match has exactly two players and a match winner.
 * The players are given as a {@link Pair} and of type {@link Player}
 * like the match winner.
 * The players are set at the beginning of the match and the match
 * winner is set at the end of the match.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class Match {

    private Pair<Player, Player> playerPair;
    private Player matchWinner;

    public Match(Pair<Player, Player> playerPair) {
        this.playerPair = playerPair;
    }

    public Player getPlayerOne() {
        return playerPair.getFirst();
    }

    public Player getPlayerTwo() {
        return playerPair.getSecond();
    }

    public Player getMatchWinner() {
        return matchWinner;
    }

    public void setMatchWinner(Player matchWinner) {
        this.matchWinner = matchWinner;
    }
}
