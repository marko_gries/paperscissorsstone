package de.gries.game.paperscissorsstone.model;

/**
 * Simple pair for the possibility to have more than one instance
 * of a type in a return of a method and to have a better connection
 * between two instances of a type.
 *
 * @author Marko Gries
 * @date 06.10.2018
 *
 * @param <S> First typ of pair.
 * @param <T> Second type of pair.
 */
public class Pair<S, T> {

    private S first;
    private T second;

    public Pair(S first, T second) {
        this.first = first;
        this.second = second;
    }

    public S getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
