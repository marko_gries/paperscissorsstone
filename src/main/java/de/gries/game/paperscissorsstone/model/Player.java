package de.gries.game.paperscissorsstone.model;

/**
 * Player for the match of game PAPER, SCISSORS, STONE.
 * A player instance has a player name, exact one {@link Symbol}
 * and a {@link PlayerName} for one {@link Match}.
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class Player {

    private Symbol symbol;
    private PlayerName playerName;

    public Player(PlayerName playerName) {
        this.playerName = playerName;
    }

    /**
     * A {@link Symbol} for a contest match.
     *
     * @return Return a dedicated {@link Symbol} to the player.
     */
    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public String getPlayerName() {
        return playerName.getNameLiterally();
    }
}
