package de.gries.game.paperscissorsstone.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import de.gries.game.paperscissorsstone.model.Match;
import de.gries.game.paperscissorsstone.model.Symbol;
import de.gries.game.paperscissorsstone.view.MatchResult;

/**
 * Tests for {@link GameService}
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @InjectMocks
    GameService gameService;

    @Spy
    GameRuleService gameRule;

    private List<MatchResult> matchResultList;
    private Match match;
    private Symbol winner;
    int strategy;
    int countOfMatches;


    @Test
    public void exactNumberOfMatchesArePlayed() {

        givenRandomStrategy();
        givenCountOfMatches();

        whenGameIsPlayed();

        thenExactNumberOfMatchesArePlayed();
    }

    @Test
    public void playerOneHasDefaultSymbol() {

        givenDefaultStrategy();

        whenMatchIsPlayed();

        thenPlayerOneHasDefaultSymbol();
    }

    @Test
    public void matchWinnerIsCorrectDetermined() {

        givenRandomStrategy();

        whenMatchIsPlayed();

        thenMatchWinnerIsCorrectDetermined();
    }


    private void givenRandomStrategy() {
        strategy = 1;
    }

    private void givenDefaultStrategy() {
        strategy = 2;
    }

    private void givenCountOfMatches() {
        countOfMatches = 10;
    }

    private void whenGameIsPlayed() {
        matchResultList = gameService.playGame(strategy, countOfMatches);
    }

    private void whenMatchIsPlayed() {
        match = gameService.playMatch(strategy);
    }

    private void thenExactNumberOfMatchesArePlayed() {
        assertThat(matchResultList).hasSize(countOfMatches);
    }

    private void thenPlayerOneHasDefaultSymbol() {
        assertThat(match.getPlayerOne()).isNotNull();
        assertThat(match.getPlayerOne().getSymbol()).isEqualTo(Symbol.getDefaultSymbol());
    }

    private void thenMatchWinnerIsCorrectDetermined() {
        Symbol playerOneSymbol = match.getPlayerOne().getSymbol();
        Symbol playerTwoSymbol = match.getPlayerTwo().getSymbol();
        Symbol winnerSymbol = match.getMatchWinner().getSymbol();
        Symbol determinedSymbol = gameRule.determineWinnerSymbol(playerOneSymbol, playerTwoSymbol);

        assertThat(winnerSymbol).isNotNull().isEqualTo(determinedSymbol);
    }
}