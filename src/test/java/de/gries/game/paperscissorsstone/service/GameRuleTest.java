package de.gries.game.paperscissorsstone.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import de.gries.game.paperscissorsstone.model.Symbol;

/**
 * Tests for the {@link GameRuleService}.
 * NOTE: the assertions are made for the rule taken from wikipedia:
 * <a href="https://de.wikipedia.org/wiki/Schere,_Stein,_Papier</a>
 *
 * @author Marko Gries
 * @date 06.10.2018
 */
public class GameRuleTest {

    private GameRuleService gameRule;
    private Symbol symbolFirst;
    private Symbol symbolSecond;
    private Symbol winnerSymbol;

    @Before
    public void before() {
        gameRule = new GameRuleService();
    }


    @Test
    public void determineWinnerSymbolForScissorsAndStone() {
        givenFirstSymbol(Symbol.SCISSORS);
        givenSecondSymbol(Symbol.STONE);

        whenDetermineWinnerSymbol();

        thenWinnerSymbolIsCorrectDeterminedTo(Symbol.STONE);
    }

    @Test
    public void determineWinnerSymbolForScissorsAndPaper() {
        givenFirstSymbol(Symbol.SCISSORS);
        givenSecondSymbol(Symbol.PAPER);

        whenDetermineWinnerSymbol();

        thenWinnerSymbolIsCorrectDeterminedTo(Symbol.SCISSORS);
    }

    @Test
    public void determineWinnerSymbolForStoneAndPaper() {
        givenFirstSymbol(Symbol.STONE);
        givenSecondSymbol(Symbol.PAPER);

        whenDetermineWinnerSymbol();

        thenWinnerSymbolIsCorrectDeterminedTo(Symbol.PAPER);
    }

    private void givenFirstSymbol(Symbol symbol) {
        symbolFirst = symbol;
    }

    public void givenSecondSymbol(Symbol symbol) {
        symbolSecond = symbol;
    }


    public void whenDetermineWinnerSymbol() {
        winnerSymbol = gameRule.determineWinnerSymbol(symbolFirst, symbolSecond);
    }

    public void thenWinnerSymbolIsCorrectDeterminedTo(Symbol expectedWinnerSymbol) {
        assertThat(winnerSymbol).isNotNull();
        assertThat(winnerSymbol).isEqualTo(expectedWinnerSymbol);
    }
}